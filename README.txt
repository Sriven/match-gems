This assignment is a match-3 style game implemented in C++ using C++ SFML/Graphics library. Please Read through the entire README before going through the code. The game from this point onwards will be referred to as MatchGems.

Note: I am adding a 1 second delay between cascading matches (using sleep()) to show the order in which cascading matches happen. So, if the cascading matches happen for a long time, the window may seem unresponsive but it's not. The matches will happen instantanously if the sleep function is removed.

C++11 REQUIRED

Game Library Used: SFML (Simple and Fast Multimedia Library)

Links (for installation):
Visual Studio: 	https://www.sfml-dev.org/tutorials/2.4/start-vc.php
Code::Blocks: 	https://www.sfml-dev.org/tutorials/2.4/start-cb.php
Linux: 		https://www.sfml-dev.org/tutorials/2.4/start-linux.php
Xcode (OSX):	https://www.sfml-dev.org/tutorials/2.4/start-osx.php


Game Rules:
MatchGems is a simple match-3 style game on a 8x8 grid. 
It consists of a single level with 30 moves. There is no limit on the time taken to make a move.
A match is made by matching 3 gems of the same shape and color vertically or horizontally. A gem can be selected and moved either vertically upwards or downwards or horizontally sidewards. This swaps the gem with another gem adjacent to the current gem, in the direction of the swap.
The swapping cannot be made if the direction of swap meets the boundry of the grid.
The swapping will reset (i.e will not occur) if no match can be made after the swap is done.
Every first match increases the score by ten points and every cascaded match after the move has been made will increase the score by twenty points.
The game ends when 30 moves are completed and and the final score is displayed on the screen.


Instructions:
The selection box in MatchGems can be moved by using the arrow keys. The left key moves the selection box left, Up key moves it upwards, Right key moves it to the right, and the Down key moves it downwards.
The selection box cannot be taken out of the grid.
Pressing the Enter key will cause a selection lock. Pressing the arrow keys again after the selection lock will swap the current gem with the adjacent gem in the direction of the arrow key pressed. The swap will not occur if this leads to swapping out of the grid.
If the Enter key is pressed by accident, pressing the escape key will free the selection box from the selection lock.
At any time, press Q to quit.


Documentation of Code:
The main logic behind the code is divided into three classes for Modularity and Extensibility.

The class Grid Encapsulates the code needed to make the grid and hold gems.
It has a contructor to initialize the objects of the class. The settexture() function sets the texture of the rectangular grid boxes as gems such that each box holds only one gem. The getint() function returns what gem the current box is holding. The setint() function sets the box to hold one type of gem.

The class rect encapsulates the code for the selection box and swapping the gems.
It has a contructor to intialize the box and set its properties. It has a number of functions which provide the necessary tools to swap and move the selection box.
The moveBox() function moves the selection box from one tile to the adjacent tile.
DrawBox() function draws the selection box to the screen. The pushdown() function is a utility function that pushes new elements into the grid after a match has been made. match3() function checks if the match has occerred either horizontally or vertically and then calls the pushdown function. The switchtex() function will be called to swap two gems positions.

The setGrid() function is called once to create the grid in the start.

The class gamelogic contains all the variables for the proper functioning of the game MatchGems. It has a contructor which initializes all the variables to their correct values.
It has return funcions which the values of initialzed variables to where its needed. It has three function which perform the main logic. The disp() function displays the grid and gems on the screen. The checker function checks if all the moves hasbeen exhausted. The GameRun() function handles all the events that take place in the window and takes all the keyboard inputs from the user.

Notes:
Haven't implemented hint system.

